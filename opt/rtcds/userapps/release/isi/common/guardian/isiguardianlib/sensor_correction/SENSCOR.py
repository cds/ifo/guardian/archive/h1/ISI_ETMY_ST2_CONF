# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from ezca.ligofilter import LIGOFilter

from .. import const as top_const
import const
import util
import time

######################################

MAX_FILTERS = 10
NUM_FILTER_ID_DIGITS = 2

        
class LIGOSensorCorrectionError(Exception): pass

_no_value = object()


class LIGOSensorCorrectionManager(object):
    """SEI EPICS interface to a standard set of Sensor Correction (SC) filters.
    
    'SC_banks' is a list of filter banks 

    'deg_of_free_list' is a list of degrees of freedom to manage the SC
    filters of.

    'ezca' is an instantiated ezca object which will be used for
    all of the channel access reads and writes that occur in this class.

    The LIGOSensorCorrectionManager manages the SC banks and the SC filters corresponding to the
    degrees of freedom in <deg_of_free_list> whose names begin with
    <ezca>.prefix + '_SENSCOR'. An instantiation of this class can transition
    all of the SC filters of the managed degrees of freedom in a specified bank and, if necessary,
    report whether a filter switch is already occurring. There is also access to the name
    of the SC filters installed for each degree of freedom in a specified bank.

    """
    def __init__(self, SC_banks, deg_of_free_list, ezca):
        self.SC_banks = SC_banks
        # self.SC_origins = SC_ORIGIN_NAMES[CHAMBER_TYPE]
        self.deg_of_free_list = deg_of_free_list
        self.ezca = ezca
        self.channel_name = const.SC_FILTER_CHANNEL_NAME
        if not util.bank_check(self.SC_banks):
            raise LIGOSensorCorrectionError("One or more of the requested banks is not valid")
            
            
    def switch_sensor_correction(self, filters_on_list, wait=const.OUTPUT_SETTLE_TIME):
        """
        Turns on only the list of filter numbers given in the bank(s) for all degrees of freedom defined on
        initialization. It will then turn OFF the rest of the filters.
        Ramps the gain down first to avoid ringing up the ISIs.
        After switching filters, if wait=True it will wait for a time, then ramp the gains back on.
        
        CAUTION!!! If wait=False, the gain does not get rampped back up on its own. You must use gain_back_up.

        Arguments:

        Filters_on_list - List of filter numbers to be turned on. Ex: [1,3,6]
        wait - Can be set to False to allow output to settle, or to a time to wait.
        """
        
        for bank in self.SC_banks:
            for deg_of_free in self.deg_of_free_list:
                SC_filter_names = self.get_SC_names(bank, deg_of_free)
                # Check there is a filter installed 
                for fm_num in filters_on_list:
                    SC_filter_name = SC_filter_names[fm_num-1]
                    if not SC_filter_name:
                        raise LIGOSensorCorrectionError("No SC filter installed in FM%d for the %s degree of freedom in the %s bank." % (fm, deg_of_free, bank))
                # Make sure that filters aren't already currently switching
                ligofilter = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=deg_of_free,bank=bank),ezca)
                if ligofilter.is_gain_ramping():
                    time.sleep(2)
                    if ligofilter.is_gain_ramping():
                        raise LIGOSensorCorrectionError('Gain is already ramping for over 15sec')

                # Start the switch
                log('Turning on {} {} bank, filters [{}]'.format(bank, deg_of_free, filters_on_list))
                # Instantiate LIGOFilter object
                ligofilter = LIGOFilter(self.channel_name.format(deg_of_free=deg_of_free,bank=bank),ezca)
                # Ramp down gain of bank
                ligofilter.ramp_gain(0, ramp_time=1, wait=True)
                # Turn on only the filters in the list
                ligofilter.only_on(['FM'+num for num in filter_on_list])
        
        # Split up here so other filters can be switched while the output settles
        if wait:
            log('Waiting for output to settle ({}sec)'.format(wait))
            # Wait for output to settle
            time.sleep(wait)
            # Bring the gain back up
            self.gain_back_up()
        

    def gain_back_up(self):
        for bank in self.SC_banks:
            for deg_of_free in self.deg_of_free_list:
                ligofilter = LIGOFilter(self.channel_name.format(deg_of_free=deg_of_free,bank=bank),ezca)
                ligofilter.ramp_gain(1, ramp_time=1, wait=True)

    
    def get_SC_names(self, bank, deg_of_free):
        """
        Gets the name of the Sensor Correction filters for each deg_of_free
        and returns a list of names installed on <deg_of_fre> SC filter
        """ 
        names = []
        for i in range(MAX_FILTERS):
            i = ('{:0'+str(NUM_FILTER_ID_DIGITS)+'d}').format(i)
            names.append(self.ezca.read(self.channel_name.format(deg_of_free=deg_of_free, bank=bank)+'_Name'+i))
                
        return names
            
    
    def get_SC_engaged(self, bank, deg_of_free):
        """
        Returns a list of tuples containing the number and name, respectively, of the engaged filter(s).
        """
        names = []
        for j in range(MAX_FILTERS):
            # if enaged, add tuple to names
            # Instantiate LIGOFilter object
            ligofilter = LIGOFilter(self.channel_name.format(deg_of_free=deg_of_free,bank=bank),ezca)
            if ligofilter.is_engaged('FM%s'%(j+1)):
                names.append((j+1, self.ezca.read(self.channel_name.format(deg_of_free=deg_of_free, bank=bank)+'_Name'+j)))
        return names
    

    @staticmethod
    def BRS_SC_switch(BRS_arg, no_BRS_coord, BRS_coord):
        """There is only one at LHO EX for now. This will either switch to using 
        the BRS Sensor Correction (ON) or not using the BRS Sensor Corrected signal (OFF).
        
        Arguments:

        BRS_arg - Either 'ON' or 'OFF' to turn the BRS SC on or off.
        no_BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would not be in a configuration to use BRS SC. Ex: (1,4)
                This matrix element will also be turned off to use BRS SC.
        BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would be in a configuration to use BRS SC. Ex: (1,7)
                This element should be turned off when not using BRS SC.
        """
        arg = BRS_arg
        if arg == 'ON':
            ezca['STS_INMTRX_{}_{}'.format(no_BRS_coord[0],no_BRS_coord[1])] = 0.0
            ezca['STS_INMTRX_{}_{}'.format(BRS_coord[0],BRS_coord[1])] = 1.0
        
        elif arg == 'OFF':
            ezca['STS_INMTRX_{}_{}'.format(no_BRS_coord[0],no_BRS_coord[1])] = 1.0
            ezca['STS_INMTRX_{}_{}'.format(BRS_coord[0],BRS_coord[1])] = 0.0
            
        else:
            raise LIGOSensorCorrectionError("Not a valid argument for BRS")


